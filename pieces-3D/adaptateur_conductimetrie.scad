/********************************************************
* Adaptateur conductimétrie
*
*   Olivier Boesch (c) 2020
*
*   v1 : version initiale
*   v2 : changement visserie M6->M3, découpe sur les côtés
********************************************************/
$fn = $preview?50:300;

//ecrou M3
d_ecrou = 6;
h_ecrou = 2.5;
l_ecrou = d_ecrou*2/sqrt(3);
d_tige = 3.7;

d_cyl_ext = 50;
h_cyl_ext = d_ecrou+2;

d_cyl_int = 35;
h_cyl_int = 0;

ent_electrodes = 15;
ep_electrodes = 3;
l_electrodes = 30;

texte = "St Ex - PC"; //"StEx 2020"
text_size = ent_electrodes/2.5;

module cyl_ext(){
    difference(){
        cylinder(d=d_cyl_ext,h=h_cyl_ext, center=true);
        //electrodes
        translate([-(ent_electrodes/2+ep_electrodes/2),0,0]) cube([ep_electrodes,l_electrodes,h_cyl_ext+2], center=true);
        translate([ent_electrodes/2+ep_electrodes/2,0,0]) cube([ep_electrodes,l_electrodes,h_cyl_ext+2], center=true);
        //fixation electrodes
        //tiges
        translate([d_cyl_ext/4+ent_electrodes/4+ep_electrodes/2,0,0]) rotate([0,90,0]) cylinder(d=d_tige, h=d_cyl_ext/2-ent_electrodes/2, center=true);
        translate([-(d_cyl_ext/4+ent_electrodes/4+ep_electrodes/2),0,0]) rotate([0,90,0]) cylinder(d=d_tige, h=d_cyl_ext/2-ent_electrodes/2, center=true);
        //ecrous
        translate([(d_cyl_ext-ent_electrodes)/4+ent_electrodes/2,0,0]) cube([h_ecrou, l_ecrou, d_ecrou], center=true);
        translate([-((d_cyl_ext-ent_electrodes)/4+ent_electrodes/2),0,0]) cube([h_ecrou, d_ecrou*2/sqrt(3),d_ecrou], center=true);
        // découpes pour tiges (pans coupés)
        translate([-d_cyl_ext/2,0,0]) cube([(d_cyl_ext-ent_electrodes)/4,d_cyl_ext+2,h_cyl_ext+2], center=true);
        translate([+d_cyl_ext/2,0,0]) cube([(d_cyl_ext-ent_electrodes)/4,d_cyl_ext+2,h_cyl_ext+2], center=true);
    }
}

module cyl_int(){
    difference(){
        cylinder(d=d_cyl_int,h=h_cyl_int, center=true);
        //electrodes
        translate([-(ent_electrodes/2+ep_electrodes/2),0,0]) cube([ep_electrodes,l_electrodes,h_cyl_int+2], center=true);
        translate([ent_electrodes/2+ep_electrodes/2,0,0]) cube([ep_electrodes,l_electrodes,h_cyl_int+2], center=true);
    }
}

module adaptateur_conductimetrie(){
    rotate([180,0,0]) union(){
        translate([0,0,h_cyl_ext/2]) cyl_ext();
        if (h_cyl_int > 0){
            translate([0,0,-h_cyl_int/2])cyl_int();
        }
        translate([0,0,-h_cyl_int]) rotate([0,180,90]) linear_extrude(1) text(texte, valign="center", halign="center", font="Futura\\-Medium:style=Medium", size = text_size);
    }
}

adaptateur_conductimetrie();
