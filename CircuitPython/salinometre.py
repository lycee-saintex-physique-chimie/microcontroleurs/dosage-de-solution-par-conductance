###############################################################################
#   
#  TP réalisation d'un salinomètre
#
#       code : Olivier Boesch
#       idée : Jérome Leboeuf
#
###############################################################################


############ Circuit
# 3.3V - Lame de cuivre ... solution ... lamme de cuivre - Résistance 10kOhm - GND
#                                                        |
#                                                       A0
##########################

import board
from analogio import AnalogIn
from utilities import Notification

# ------------ parametres (pour le professeur) ----------
broche_entree = board.A0  # broche d'entree du potentiomètre
resistance_pont = 10000  # Ohms - résistance du pont

# -------------------------------------------------------


# ################## MODELES A MODIFIER PAR L'ELEVE ############################
# calcul de la resistance
def calcul_conductance(u_entree):
    # on calcule la résistance
    r = resistance_pont * (3.3 / u_entree - 1)
    # la conductance est l'inverse de la résistance
    g=1/r
    return g


# calcul de la concentration (modèle trouvé par vous)
def calcul_concentration(g):
    Cm = g * 1.0
    return Cm


# ############################################################################


# configuration de l'entrée analogique
entree_analogique = AnalogIn(broche_entree)
# config des notifications
notif = Notification()
# affichage du logo (pendant 1s)
notif.oled_logo('saintex_logo.bin')
time.sleep(1)

# boucle
while True:
    # calculer la tension à l'entrée analogique
    tension = entree_analogique.value * 3.3 / 65535
    # calculer la conductance de la solution
    G = calcul_conductance(tension)
    # calculer la concentration par le modèle
    Cm = calcul_concentration(G)
    # afficher les valeurs (lcd ou serie)
    notif.notify(text='ULames={:3.2f}V\nG={:3.2e}S\nCm={:3.2f}g/L'.format(tension, G, Cm))
    # attendre 1s
    time.sleep(1)
